package com.carCharging.services;

import com.carCharging.carChargingSummary.SummaryCalculator;
import com.carCharging.errorHandling.exceptions.CarChargingSessionNotFoundException;
import com.carCharging.models.CarChargingRequest;
import com.carCharging.models.CarChargingResponse;
import com.carCharging.models.CarChargingSession;
import com.carCharging.models.ChargingStatus;
import com.carCharging.models.summary.CarChargingSummary;
import com.carCharging.models.summary.CarChargingSummaryResponse;
import com.carCharging.storage.CarChargingDataStorage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class CarChargingServiceTest {
    private static final String STATION_ID = "ABC-12345";
    private static final LocalDateTime STARTED_AT = LocalDateTime.now();
    private static final String ID = "d9bb7458-d5d9-4de7-87f7-7f39edd51d18";
    private static final String ANOTHER_ID = "cd38432c-f054-11e9-81b4-2a2ae2dbcce4";

    private static final LocalDateTime REQUEST_TIME = LocalDateTime.now();

    private static final CarChargingRequest REQUEST = new CarChargingRequest(STATION_ID);


    @Mock
    private static CarChargingDataStorage dataStorage;

    @Mock
    private static SummaryCalculator summaryCalculator;

    private CarChargingService service;
    public static final CarChargingSummary CHARGING_SUMMARY = new CarChargingSummary(3L, 1L, 2L);

    @Before
    public void setUp() {
        initMocks(this);

        service = new CarChargingService(dataStorage, summaryCalculator);

        CarChargingSession session = getCarChargingSession(ID);
        CarChargingSession anotherSession = getCarChargingSession(ANOTHER_ID);

        when(dataStorage.add(ArgumentMatchers.any(CarChargingSession.class))).thenReturn(session);
        when(dataStorage.findAll()).thenReturn(Arrays.asList(session, anotherSession));
        when(dataStorage.findById(ID)).thenReturn(session);
        when(summaryCalculator.calculateSummary(eq(REQUEST_TIME), anyCollection())).thenReturn(CHARGING_SUMMARY);
        doNothing().when(dataStorage).update(session);
    }

    @Test
    public void persistsCarChargingSession() {
        service.create(REQUEST);

        ArgumentCaptor<CarChargingSession> sessionArgumentCaptor = ArgumentCaptor.forClass(CarChargingSession.class);

        verify(dataStorage).add(sessionArgumentCaptor.capture());

        CarChargingSession storedChargingSession = sessionArgumentCaptor.getValue();

        assertThat(storedChargingSession.getStationId(), is(STATION_ID));
        assertThat(storedChargingSession.getStatus(), is(ChargingStatus.IN_PROGRESS));
    }

    @Test
    public void shouldReturnCarChargingResponse() {
        CarChargingResponse response = service.create(REQUEST);

        assertThat(response.getStationId(), is(REQUEST.getStationId()));
        assertThat(response.getStatus(), is(ChargingStatus.IN_PROGRESS));
        assertThat(response.getId(), is(ID));
        assertThat(response.getStartedAt(), is(STARTED_AT));
    }

    @Test
    public void fetchesAllCarChargingSessions() {
        service.retrieveAll();

        verify(dataStorage).findAll();
    }

    @Test
    public void retrievesAllCarChargingSessions() {
        List<CarChargingResponse> response = service.retrieveAll();

        assertThat(response, hasSize(2));

        CarChargingResponse sessionResponse = response.get(0);
        assertThat(sessionResponse.getId(), is(ID));
        assertThat(sessionResponse.getStartedAt(), is(STARTED_AT));
        assertThat(sessionResponse.getStationId(), is(STATION_ID));
        assertThat(sessionResponse.getStatus(), is(ChargingStatus.IN_PROGRESS));

        assertThat(response.get(1).getId(), is(ANOTHER_ID));
    }

    @Test
    public void retrievesCarChargingSessionToBeUpdated() {
        service.stopChargingSession(ID);

        verify(dataStorage).findById(ID);
    }

    @Test
    public void updateCarChargingSessionToFinishedOnStopSession() {
        service.stopChargingSession(ID);

        ArgumentCaptor<CarChargingSession> sessionArgumentCaptor = ArgumentCaptor.forClass(CarChargingSession.class);

        verify(dataStorage).update(sessionArgumentCaptor.capture());

        CarChargingSession sessionToBeUpdated = sessionArgumentCaptor.getValue();

        assertThat(sessionToBeUpdated.getStatus(), is(ChargingStatus.FINISHED));
        assertThat(sessionToBeUpdated.getStoppedAt(), is(notNullValue()));
    }

    @Test
    public void returnsStoppedChargingSession() {
        CarChargingResponse response = service.stopChargingSession(ID);

        assertThat(response.getId(), is(ID));
        assertThat(response.getStatus(), is(ChargingStatus.FINISHED));
        assertThat(response.getStationId(), is(STATION_ID));
        assertThat(response.getStartedAt(), is(STARTED_AT));
        assertThat(response.getStoppedAt(), is(notNullValue()));
    }

    @Test(expected = CarChargingSessionNotFoundException.class)
    public void throwsCarChargingSessionNotFoundExceptionWhenSessionToUpdateDoesNotExist() {
        when(dataStorage.findById(ID)).thenReturn(null);

        service.stopChargingSession(ID);
    }

    @Test
    public void retrievesAllCarChargingSessionsToHaveItsSummaryCalculated() {
        service.calculateSummary(REQUEST_TIME);

        verify(dataStorage).findAll();
    }

    @Test
    public void calculatesChargingSessionSummary() {
        service.calculateSummary(REQUEST_TIME);

        verify(summaryCalculator).calculateSummary(eq(REQUEST_TIME), anyCollection());
    }

    @Test
    public void returnsSummaryResponse() {
        CarChargingSummaryResponse summaryResponse = service.calculateSummary(REQUEST_TIME);

        assertThat(summaryResponse.getStartedCount(), is(CHARGING_SUMMARY.getStartedCount()));
        assertThat(summaryResponse.getStoppedCount(), is(CHARGING_SUMMARY.getStoppedCount()));
        assertThat(summaryResponse.getTotalCount(), is(CHARGING_SUMMARY.getTotalCount()));
    }

    private static CarChargingSession getCarChargingSession(String id) {
        return new CarChargingSession(id, STATION_ID, STARTED_AT, ChargingStatus.IN_PROGRESS);
    }
}