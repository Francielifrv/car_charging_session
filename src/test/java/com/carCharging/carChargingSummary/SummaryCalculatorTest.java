package com.carCharging.carChargingSummary;

import com.carCharging.carChargingSummary.SummaryCalculator;
import com.carCharging.models.CarChargingSession;
import com.carCharging.models.ChargingStatus;
import com.carCharging.models.summary.CarChargingSummary;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class SummaryCalculatorTest {
    private static final LocalDateTime NOW = LocalDateTime.now();

    private SummaryCalculator summaryCalculator;

    @Before
    public void setUp() {
        summaryCalculator = new SummaryCalculator();
    }

    @Test
    public void shouldCalculateAmountOfSessionsStartedOneMinuteBeforeTime() {
        Collection<CarChargingSession> sessions = getCarChargingSessionsInProgress();

        CarChargingSummary summary = summaryCalculator.calculateSummary(NOW, sessions);

        assertThat(summary.getStartedCount(), is(4L));
    }

    @Test
    public void shouldCalculateAmountOfSessionsStoppedOneMinuteBeforeTime() {
        Collection<CarChargingSession> sessions = getFinishedCarChargingSessions();

        CarChargingSummary summary = summaryCalculator.calculateSummary(NOW, sessions);

        assertThat(summary.getStoppedCount(), is(2L));
    }

    @Test
    public void shouldCalculateAmountOfSessionsBothStartedOrStoppedOneMinuteBeforeTime() {
        Collection<CarChargingSession> sessions = getFinishedAndStartedCarChargingSessions();

        CarChargingSummary summary = summaryCalculator.calculateSummary(NOW, sessions);

        assertThat(summary.getTotalCount(), is(3L));
    }

    private Collection<CarChargingSession> getCarChargingSessionsInProgress() {
        return Arrays.asList(
                createSessionInProgress(NOW.minusSeconds(30)),
                createSessionInProgress(NOW.minusSeconds(59)),
                createSessionInProgress(NOW.minusSeconds(5)),
                createSessionInProgress(NOW.minusSeconds(2)),
                createSessionInProgress(NOW.minusMinutes(23)),
                createSessionInProgress(NOW.minusMinutes(5)),
                createSessionInProgress(NOW.plusSeconds(30))
        );
    }

    private Collection<CarChargingSession> getFinishedCarChargingSessions() {
        return Arrays.asList(
                createFinishedChargingSession(NOW.minusSeconds(1)),
                createFinishedChargingSession(NOW.minusSeconds(43)),
                createFinishedChargingSession(NOW.minusSeconds(365)),
                createFinishedChargingSession(NOW.minusMinutes(5)),
                createFinishedChargingSession(NOW.minusMinutes(23)),
                createFinishedChargingSession(NOW.plusSeconds(30)),
                createFinishedChargingSession(NOW.plusMinutes(22))
        );
    }

    private Collection<CarChargingSession> getFinishedAndStartedCarChargingSessions() {
        return Arrays.asList(
                createSessionInProgress(NOW.minusSeconds(30)),
                createFinishedChargingSession(NOW.minusSeconds(1)),
                createFinishedChargingSession(NOW.minusSeconds(43)),
                createFinishedChargingSession(NOW.minusSeconds(365)),
                createFinishedChargingSession(NOW.plusMinutes(22))
        );
    }

    private static CarChargingSession createSessionInProgress(LocalDateTime startedAt) {
        return createSession(startedAt, null, ChargingStatus.IN_PROGRESS);
    }

    private static CarChargingSession createFinishedChargingSession(LocalDateTime stoppedAt) {
        return createSession(null, stoppedAt, ChargingStatus.FINISHED);
    }

    private static CarChargingSession createSession(LocalDateTime startTime, LocalDateTime endTime, ChargingStatus status) {
        return new CarChargingSession("id", "station_id", startTime, endTime, status);
    }
}