package com.carCharging.storage;

import com.carCharging.models.CarChargingSession;
import com.carCharging.models.ChargingStatus;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.carCharging.models.ChargingStatus.IN_PROGRESS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class CarChargingDataStorageTest {
    private static final String ID = "d9bb7458-d5d9-4de7-87f7-7f39edd51d18";
    private static final String STATION_ID = "ABC-12345";
    private static final LocalDateTime STARTED_AT = LocalDateTime.now();
    private static final LocalDateTime STOPPED_AT = LocalDateTime.now().plus(10, MINUTES);

    private static CarChargingSession session;
    private static CarChargingDataStorage storage;

    @Before
    public void setUp() {
        storage = new CarChargingDataStorage();

        session = getChargingSession(ID);
    }

    @Test
    public void shouldRetrieveSingleItemById() {
        CarChargingDataStorage storage = new CarChargingDataStorage();

        storage.add(session);

        CarChargingSession retrievedSession = storage.findById(ID);

        assertThat(retrievedSession, is(session));
    }

    @Test
    public void retrievesAllCarSessionsAsList() {
        CarChargingSession anotherSession = getChargingSession("cd38432c-f054-11e9-81b4-2a2ae2dbcce4");

        storage.add(session);
        storage.add(anotherSession);

        Collection<CarChargingSession> carChargingSessions = storage.findAll();

        assertThat(carChargingSessions, hasSize(2));
        assertThat(carChargingSessions, hasItems(session, anotherSession));
    }

    @Test
    public void updateCarChargingSession() {
        ConcurrentMap<String, CarChargingSession> data = mock(ConcurrentHashMap.class);

        CarChargingDataStorage dataStorage = new CarChargingDataStorage(data);

        dataStorage.update(session);

        verify(data).replace(session.getId(), session);
    }

    private CarChargingSession getChargingSession(String id) {
        return new CarChargingSession(id, STATION_ID, STARTED_AT, STOPPED_AT, IN_PROGRESS);
    }
}