package com.carCharging.models;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class CarChargingSessionTest {
    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final LocalDateTime THIRTY_SECONDS_AGO = NOW.minusSeconds(30);
    private static final LocalDateTime TWO_MINUTES_AGO = NOW.minusMinutes(2);
    private static final LocalDateTime TWO_MINUTES_ON_FUTURE = NOW.plusMinutes(2);

    @Test
    public void cratesChargingSessionFromRequest() {
        CarChargingRequest request = new CarChargingRequest("ABC-12345");

        CarChargingSession session = CarChargingSession.fromRequest(request);

        assertThat(session.getStationId(), is(request.getStationId()));
        assertThat(session.getStartedAt(), is(notNullValue()));
        assertThat(session.getId(), is(notNullValue()));
        assertThat(session.getStatus(), is(ChargingStatus.IN_PROGRESS));
        assertThat(session.getStoppedAt(), is(nullValue()));
        assertThat(session.getStoppedAt(), is(nullValue()));
    }

    @Test
    public void returnsFalseWhenChargingSessionStartedMoreThanOneMinuteAfterGivenTime() {
        CarChargingSession session = createCarChargingSession(TWO_MINUTES_AGO, null, ChargingStatus.IN_PROGRESS);

        assertThat(session.hasStartedOneMinuetBefore(NOW), is(false));
    }

    @Test
    public void returnsFalseWhenChargingSessionStartedLessThanOneMinuteBeforeGivenTimeButStatusIsNotInProgress() {
        CarChargingSession session = createCarChargingSession(THIRTY_SECONDS_AGO, null, ChargingStatus.FINISHED);

        assertThat(session.hasStartedOneMinuetBefore(NOW), is(false));
    }

    @Test
    public void returnsTrueWhenChargingSessionHasStartedOneMinuteBeforeGivenTimeAndStatusIsInProgress() {
        CarChargingSession session = createCarChargingSession(THIRTY_SECONDS_AGO, null, ChargingStatus.IN_PROGRESS);

        assertThat(session.hasStartedOneMinuetBefore(NOW), is(true));
    }

    @Test
    public void handlesNullStartedAtWhenCheckingIfSessionStartedBeforeTime() {
        CarChargingSession session = createCarChargingSession(null, null, ChargingStatus.IN_PROGRESS);

        assertThat(session.hasStartedOneMinuetBefore(NOW), is(false));
    }

    @Test
    public void returnsFalseWhenSessionStartedAfterGivenTime() {
        CarChargingSession session = createCarChargingSession(TWO_MINUTES_ON_FUTURE, null, ChargingStatus.IN_PROGRESS);

        assertThat(session.hasStartedOneMinuetBefore(NOW), is(false));
    }

    @Test
    public void returnsFalseWhenChargingSessionFinishedMoreThanOneMinuteAfterGivenTime() {
        CarChargingSession session = createCarChargingSession(null, TWO_MINUTES_AGO, ChargingStatus.FINISHED);

        assertThat(session.hasStoppedOneMinuteBefore(NOW), is(false));
    }

    @Test
    public void returnsFalseWhenChargingSessionFinishedLessThanOneMinuteBeforeGivenTimeButStatusIsNotFinished() {
        CarChargingSession session = createCarChargingSession(null, THIRTY_SECONDS_AGO, ChargingStatus.IN_PROGRESS);

        assertThat(session.hasStoppedOneMinuteBefore(NOW), is(false));
    }

    @Test
    public void returnsTrueWhenChargingSessionHasFinishedOneMinuteBeforeGivenTimeAndStatusIsFinished() {
        CarChargingSession session = createCarChargingSession(null, THIRTY_SECONDS_AGO, ChargingStatus.FINISHED);

        assertThat(session.hasStoppedOneMinuteBefore(NOW), is(true));
    }

    @Test
    public void handlesNullStoppedAtWhenCheckingIfSessionFinishedBeforeTime() {
        CarChargingSession session = createCarChargingSession(null, null, ChargingStatus.FINISHED);

        assertThat(session.hasStoppedOneMinuteBefore(NOW), is(false));
    }

    @Test
    public void returnsFalseWhenSessionFinishedAfterGivenTime() {
        CarChargingSession session = createCarChargingSession(null, TWO_MINUTES_ON_FUTURE, ChargingStatus.FINISHED);

        assertThat(session.hasStartedOneMinuetBefore(NOW), is(false));
    }

    @Test
    public void returnsTrueWhenSessionStartedUpToOneMinuteBeforeGivenTime() {
        CarChargingSession session = createCarChargingSession(THIRTY_SECONDS_AGO, null, ChargingStatus.IN_PROGRESS);

        assertThat(session.hasStatedOrFinishedUptToOneMinuteBefore(NOW), is(true));
    }

    @Test
    public void returnsTrueWhenSessionFinishedUpToOneMinuteBeforeGivenTime() {
        CarChargingSession session = createCarChargingSession(null, THIRTY_SECONDS_AGO, ChargingStatus.FINISHED);

        assertThat(session.hasStatedOrFinishedUptToOneMinuteBefore(NOW), is(true));
    }

    @Test
    public void returnsFalseWhenSessionStartedAndFinishedMoreThanOneMinuteBeforeGivenTime() {
        CarChargingSession sessionInProgress = createCarChargingSession(TWO_MINUTES_AGO, TWO_MINUTES_AGO,
                ChargingStatus.FINISHED);
        assertThat(sessionInProgress.hasStatedOrFinishedUptToOneMinuteBefore(NOW), is(false));

        CarChargingSession finishedSession = createCarChargingSession(TWO_MINUTES_AGO, TWO_MINUTES_AGO,
                ChargingStatus.IN_PROGRESS);

        assertThat(finishedSession.hasStatedOrFinishedUptToOneMinuteBefore(NOW), is(false));
    }

    @Test
    public void returnsFalseWhenSessionStartedAndFinishedMoreThanOneMinuteAfterGivenTime() {
        CarChargingSession sessionInProgress = createCarChargingSession(TWO_MINUTES_ON_FUTURE, TWO_MINUTES_ON_FUTURE,
                ChargingStatus.FINISHED);
        CarChargingSession finishedSession = createCarChargingSession(TWO_MINUTES_ON_FUTURE, TWO_MINUTES_ON_FUTURE,
                ChargingStatus.IN_PROGRESS);

        assertThat(sessionInProgress.hasStatedOrFinishedUptToOneMinuteBefore(NOW), is(false));
        assertThat(finishedSession.hasStatedOrFinishedUptToOneMinuteBefore(NOW), is(false));
    }

    private static CarChargingSession createCarChargingSession(LocalDateTime startedAt, LocalDateTime stoppedAt,
                                                               ChargingStatus status) {
        return new CarChargingSession("id", "stationId", startedAt, stoppedAt, status);
    }
}