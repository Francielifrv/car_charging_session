package com.carCharging.models;

import com.carCharging.models.CarChargingResponse;
import com.carCharging.models.CarChargingSession;
import com.carCharging.models.ChargingStatus;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.LongSummaryStatistics;

import static java.time.temporal.ChronoUnit.MINUTES;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class CarChargingResponseTest {
    private static final String ID = "d9bb7458-d5d9-4de7-87f7-7f39edd51d18";
    private static final String STATION_ID = "ABC-12345";
    private static final LocalDateTime STARTED_AT = LocalDateTime.now();
    private static final LocalDateTime STOPPED_AT = STARTED_AT.plus(2, MINUTES);

    @Test
    public void shouldBeCreatedFromCarChargingSession() {
        CarChargingSession session = new CarChargingSession(ID, STATION_ID, STARTED_AT, STOPPED_AT,
                ChargingStatus.IN_PROGRESS);

        CarChargingResponse response = CarChargingResponse.fromChargingSession(session);

        assertThat(response.getId(), is(ID));
        assertThat(response.getStationId(), is(STATION_ID));
        assertThat(response.getStartedAt(), is(STARTED_AT));
        assertThat(response.getStatus(), is(ChargingStatus.IN_PROGRESS));
        assertThat(response.getStoppedAt(), is(notNullValue()));
    }
}