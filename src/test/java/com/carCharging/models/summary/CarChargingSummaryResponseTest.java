package com.carCharging.models.summary;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class CarChargingSummaryResponseTest {
    @Test
    public void convertsCarChargingSummaryToResponseObject() {
        CarChargingSummary summary = new CarChargingSummary(9L, 5L, 4L);

        CarChargingSummaryResponse summaryResponse =  CarChargingSummaryResponse.fromChargingSummary(summary);

        assertThat(summaryResponse.getTotalCount(), is(summary.getTotalCount()));
        assertThat(summaryResponse.getStoppedCount(), is(summary.getStoppedCount()));
        assertThat(summaryResponse.getStartedCount(), is(summary.getStartedCount()));
    }
}