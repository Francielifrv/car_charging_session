package com.carCharging.controllers;

import com.carCharging.models.CarChargingRequest;
import com.carCharging.models.CarChargingResponse;
import com.carCharging.models.ChargingStatus;
import com.carCharging.models.summary.CarChargingSummaryResponse;
import com.carCharging.services.CarChargingService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class CarChargingControllerTest {

    private final String STATION_ID = "ABC-12345";
    private final LocalDateTime STARTED_AT = LocalDateTime.now();
    private final String ID = "d9bb7458-d5d9-4de7-87f7-7f39edd51d18";

    private final CarChargingRequest CREATE_CHARGING_CAR_REQUEST = new CarChargingRequest(STATION_ID);
    private final CarChargingResponse CHARGING_CAR_RESPONSE = createCarChargingResponse();

    @Mock
    private CarChargingService service;

    private CarChargingController controller;

    @Before
    public void setUp() {
        initMocks(this);

        when(service.create(CREATE_CHARGING_CAR_REQUEST)).thenReturn(CHARGING_CAR_RESPONSE);
        when(service.retrieveAll()).thenReturn(singletonList(CHARGING_CAR_RESPONSE));
        when(service.stopChargingSession(ID)).thenReturn(CHARGING_CAR_RESPONSE);

        controller = new CarChargingController(service);
    }


    @Test
    public void createsNewChargingSession() {
        controller.create(CREATE_CHARGING_CAR_REQUEST);

        verify(service).create(CREATE_CHARGING_CAR_REQUEST);
    }

    @Test
    public void returnsResponseBodyWhenChargingSessionIsCreated() {
        CarChargingResponse response = controller.create(CREATE_CHARGING_CAR_REQUEST);

        assertThat(response.getId(), is(ID));
        assertThat(response.getStationId(), is(CREATE_CHARGING_CAR_REQUEST.getStationId()));
        assertThat(response.getStartedAt(), is(STARTED_AT));
        assertThat(response.getStatus(), is(ChargingStatus.IN_PROGRESS));
    }

    @Test
    public void fetchesAllCarChargingSessions() {
        controller.retrieveAll();

        verify(service).retrieveAll();
    }

    @Test
    public void returnsAllCarChargingSessions() {
        List<CarChargingResponse> response = controller.retrieveAll();

        assertThat(response, hasSize(1));
        assertThat(response, hasItem(CHARGING_CAR_RESPONSE));
    }

    @Test
    public void stopsCarChargingSession() {
        controller.stopChargingSession(ID);

        ArgumentCaptor<String> idArgumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(service).stopChargingSession(idArgumentCaptor.capture());

        String id = idArgumentCaptor.getValue();
        assertThat(id, is(ID));
    }

    @Test
    public void returnsStoppedChargingSession() {
        CarChargingResponse response = controller.stopChargingSession(ID);

        assertThat(response, is(CHARGING_CAR_RESPONSE));
    }

    @Test
    public void calculatesCarChargingSessionSummary() {
        CarChargingSummaryResponse summaryResponse = new CarChargingSummaryResponse(3L, 1L, 2L);
        when(service.calculateSummary(ArgumentMatchers.any(LocalDateTime.class))).thenReturn(summaryResponse);

        CarChargingSummaryResponse response = controller.getSummary();

        verify(service).calculateSummary(ArgumentMatchers.any(LocalDateTime.class));
        assertThat(response, is(summaryResponse));
    }

    private CarChargingResponse createCarChargingResponse() {
        return new CarChargingResponse(ID, STATION_ID, STARTED_AT, null, ChargingStatus.IN_PROGRESS);
    }
}