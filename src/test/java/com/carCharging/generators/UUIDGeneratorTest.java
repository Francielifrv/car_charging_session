package com.carCharging.generators;

import org.junit.Test;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.junit.Assert.assertThat;

public class UUIDGeneratorTest {
    private final String UUID_REGEX = "[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}";

    @Test
    public void generatesUUIDAsString() {
        String uuid = UUIDGenerator.generate();

        assertThat(uuid, matchesPattern(UUID_REGEX));
    }

    @Test
    public void UUIDsAreRandom() {
        String uuid = UUIDGenerator.generate();
        String anotherUuid = UUIDGenerator.generate();

        assertThat(uuid, is(not(anotherUuid)));
    }
}