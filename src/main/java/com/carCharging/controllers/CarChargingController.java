package com.carCharging.controllers;

import com.carCharging.models.CarChargingRequest;
import com.carCharging.models.CarChargingResponse;
import com.carCharging.models.summary.CarChargingSummaryResponse;
import com.carCharging.services.CarChargingService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/chargingSessions")
public class CarChargingController {
    private CarChargingService service;

    @Autowired
    public CarChargingController(CarChargingService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseBody
    public CarChargingResponse create(@Valid @RequestBody CarChargingRequest request) {
        return service.create(request);
    }

    @GetMapping
    @ResponseBody
    public List<CarChargingResponse> retrieveAll() {
        return service.retrieveAll();
    }

    @PutMapping("/{id}")
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse( code = 200, message = "OK"),
            @ApiResponse(code= 400, message = "Bad request"),
            @ApiResponse(code= 404, message = "Not found")
    })
    public CarChargingResponse stopChargingSession(@PathVariable(value = "id") String id) {
        return service.stopChargingSession(id);
    }

    @GetMapping("/summary")
    @ResponseBody
    public CarChargingSummaryResponse getSummary() {
        LocalDateTime requestTime = LocalDateTime.now();

        return service.calculateSummary(requestTime);
    }
}