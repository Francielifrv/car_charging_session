package com.carCharging.errorHandling;

import com.carCharging.errorHandling.exceptions.CarChargingSessionNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler
    public ResponseEntity handleCarChargingSessionNotFoundException(CarChargingSessionNotFoundException exception) {
        String errorMessage = String.format("{message: %s}", exception.getMessage());

        return ResponseEntity.status(exception.getHttpStatus()).body(errorMessage);
    }
}
