package com.carCharging.errorHandling.exceptions;

import org.springframework.http.HttpStatus;

public class CarChargingSessionNotFoundException extends RuntimeException {
    private HttpStatus httpStatus = HttpStatus.NOT_FOUND;

    public CarChargingSessionNotFoundException(String message) {
        super(message);
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
