package com.carCharging.services;

import com.carCharging.carChargingSummary.SummaryCalculator;
import com.carCharging.errorHandling.exceptions.CarChargingSessionNotFoundException;
import com.carCharging.models.CarChargingRequest;
import com.carCharging.models.CarChargingResponse;
import com.carCharging.models.CarChargingSession;
import com.carCharging.models.ChargingStatus;
import com.carCharging.models.summary.CarChargingSummary;
import com.carCharging.models.summary.CarChargingSummaryResponse;
import com.carCharging.storage.CarChargingDataStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarChargingService {

    private CarChargingDataStorage dataStorage;
    private SummaryCalculator summaryCalculator;

    @Autowired
    public CarChargingService(CarChargingDataStorage dataStorage, SummaryCalculator summaryCalculator) {
        this.dataStorage = dataStorage;
        this.summaryCalculator = summaryCalculator;
    }

    public CarChargingResponse create(CarChargingRequest request) {
        CarChargingSession session = CarChargingSession.fromRequest(request);

        CarChargingSession storedSession = dataStorage.add(session);

        return CarChargingResponse.fromChargingSession(storedSession);
    }

    public List<CarChargingResponse> retrieveAll() {
        Collection<CarChargingSession> carSessions = dataStorage.findAll();

        return toResponseList(carSessions);
    }

    private List<CarChargingResponse> toResponseList(Collection<CarChargingSession> carSessions) {
        return carSessions.stream().map(CarChargingResponse::fromChargingSession).collect(Collectors.toList());
    }

    public CarChargingResponse stopChargingSession(String id) {
        CarChargingSession session = dataStorage.findById(id);

        if (session == null) {
            throw new CarChargingSessionNotFoundException("Could not find car charging session with id: " + id);
        }

        session.setStatus(ChargingStatus.FINISHED);
        session.setStoppedAt(LocalDateTime.now());

        dataStorage.update(session);

        return CarChargingResponse.fromChargingSession(session);
    }

    public CarChargingSummaryResponse calculateSummary(LocalDateTime requestTime) {
        Collection<CarChargingSession> chargingSessions = dataStorage.findAll();

        CarChargingSummary summary = summaryCalculator.calculateSummary(requestTime, chargingSessions);

        return CarChargingSummaryResponse.fromChargingSummary(summary);
    }
}
