package com.carCharging.models;

import com.carCharging.generators.UUIDGenerator;

import java.time.Duration;
import java.time.LocalDateTime;

public class CarChargingSession {
    private String id;
    private String stationId;
    private LocalDateTime startedAt;
    private LocalDateTime stoppedAt;
    private ChargingStatus status;

    public CarChargingSession(String id, String stationId, LocalDateTime startedAt, LocalDateTime stoppedAt, ChargingStatus status) {
        this.id = id;
        this.stationId = stationId;
        this.startedAt = startedAt;
        this.stoppedAt = stoppedAt;
        this.status = status;
    }

    public CarChargingSession(String id, String stationId, LocalDateTime startedAt, ChargingStatus chargingStatus) {
        this(id, stationId, startedAt, null, chargingStatus);
    }

    public CarChargingSession(String stationId, LocalDateTime startedAt, ChargingStatus chargingStatus) {
        this(UUIDGenerator.generate(), stationId, startedAt, null, chargingStatus);
    }

    public static CarChargingSession fromRequest(CarChargingRequest request) {
        return new CarChargingSession(request.getStationId(), LocalDateTime.now(), ChargingStatus.IN_PROGRESS);
    }

    public String getId() {
        return id;
    }

    public String getStationId() {
        return stationId;
    }

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public LocalDateTime getStoppedAt() {
        return stoppedAt;
    }

    public ChargingStatus getStatus() {
        return status;
    }

    public void setStoppedAt(LocalDateTime stoppedAt) {
        this.stoppedAt = stoppedAt;
    }

    public void setStatus(ChargingStatus status) {
        this.status = status;
    }

    public boolean hasStartedOneMinuetBefore(LocalDateTime time) {
        return null != startedAt && isInProgress() && hasStartedUpToOneMinuteBefore(time);
    }

    private boolean isInProgress() {
        return status.equals(ChargingStatus.IN_PROGRESS);
    }

    public boolean hasStoppedOneMinuteBefore(LocalDateTime time) {
        return null != stoppedAt && isFinished() && hasStoppedUpToOneMinuteBefore(time);
    }

    private boolean isFinished() {
        return status.equals(ChargingStatus.FINISHED);
    }

    private boolean hasStartedUpToOneMinuteBefore(LocalDateTime time) {
        return hasOneMinuteDifference(startedAt, time);
    }

    private boolean hasStoppedUpToOneMinuteBefore(LocalDateTime time) {
        return hasOneMinuteDifference(stoppedAt, time);
    }

    private boolean hasOneMinuteDifference(LocalDateTime time, LocalDateTime anotherTime) {
        long timeDifference = Duration.between(time, anotherTime).getSeconds();

        return timeDifference > 0 && timeDifference <= 60;
    }

    public boolean hasStatedOrFinishedUptToOneMinuteBefore(LocalDateTime time) {
        return hasStartedOneMinuetBefore(time) || hasStoppedOneMinuteBefore(time);
    }
}

