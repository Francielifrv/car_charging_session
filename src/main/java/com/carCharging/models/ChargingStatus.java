package com.carCharging.models;

public enum ChargingStatus {
    IN_PROGRESS, FINISHED
}
