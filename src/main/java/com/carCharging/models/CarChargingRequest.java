package com.carCharging.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class CarChargingRequest {

    @NotBlank
    private final String stationId;

    public CarChargingRequest(@JsonProperty("stationId") String stationId) {
        this.stationId = stationId;
    }

    public String getStationId() {
        return stationId;
    }
}
