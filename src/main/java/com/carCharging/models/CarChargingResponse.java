package com.carCharging.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class CarChargingResponse {
    @JsonProperty("id")
    private final String id;

    @JsonProperty("stationId")
    private final String stationId;

    @JsonProperty("startedAt")
    private final LocalDateTime startedAt;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("stoppedAt")
    private final LocalDateTime stoppedAt;

    @JsonProperty("status")
    private final ChargingStatus status;

    public CarChargingResponse(String id, String stationId, LocalDateTime startedAt, LocalDateTime stoppedAt,
                               ChargingStatus status) {
        this.id = id;
        this.stationId = stationId;
        this.startedAt = startedAt;
        this.stoppedAt = stoppedAt;
        this.status = status;
    }

    public static CarChargingResponse fromChargingSession(CarChargingSession session) {
        return new CarChargingResponse(session.getId(), session.getStationId(), session.getStartedAt(),
                session.getStoppedAt(), session.getStatus());
    }

    public String getId() {
        return id;
    }

    public String getStationId() {
        return stationId;
    }

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public ChargingStatus getStatus() {
        return status;
    }

    public LocalDateTime getStoppedAt() {
        return stoppedAt;
    }
}
