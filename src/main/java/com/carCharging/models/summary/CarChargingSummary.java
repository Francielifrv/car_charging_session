package com.carCharging.models.summary;

public class CarChargingSummary {
    private final Long totalCount;
    private final Long startedCount;
    private final Long stoppedCount;

    public CarChargingSummary(Long totalCount, Long startedCount, Long stoppedCount) {
        this.totalCount = totalCount;
        this.startedCount = startedCount;
        this.stoppedCount = stoppedCount;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public Long getStartedCount() {
        return startedCount;
    }

    public Long getStoppedCount() {
        return stoppedCount;
    }
}
