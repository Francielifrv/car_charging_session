package com.carCharging.models.summary;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CarChargingSummaryResponse {

    @JsonProperty("totalCount")
    private final Long totalCount;

    @JsonProperty("startedCount")
    private final Long startedCount;

    @JsonProperty("stoppedCount")
    private final Long stoppedCount;

    public CarChargingSummaryResponse(Long totalCount, Long startedCount, Long stoppedCount) {
        this.totalCount = totalCount;
        this.startedCount = startedCount;
        this.stoppedCount = stoppedCount;
    }

    public static CarChargingSummaryResponse fromChargingSummary(CarChargingSummary summary) {
        return new CarChargingSummaryResponse(summary.getTotalCount(), summary.getStartedCount(), summary.getStoppedCount());
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public Long getStartedCount() {
        return startedCount;
    }

    public Long getStoppedCount() {
        return stoppedCount;
    }
}
