package com.carCharging.storage;

import com.carCharging.models.CarChargingSession;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class CarChargingDataStorage {
    private ConcurrentMap<String, CarChargingSession> storage;

    public CarChargingDataStorage() {
        this.storage = new ConcurrentHashMap<>();
    }

    protected CarChargingDataStorage(ConcurrentMap<String, CarChargingSession> storage) {
        this.storage = storage;
    }

    public CarChargingSession add(CarChargingSession carChargingSession) {
        storage.putIfAbsent(carChargingSession.getId(), carChargingSession);

        return carChargingSession;
    }

    public CarChargingSession findById(String id) {
        return storage.get(id);
    }

    public Collection<CarChargingSession> findAll() {
        return storage.values();
    }

    public void update(CarChargingSession session) {
        storage.replace(session.getId(), session);
    }
}
