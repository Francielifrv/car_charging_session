package com.carCharging.carChargingSummary;

import com.carCharging.models.CarChargingSession;
import com.carCharging.models.summary.CarChargingSummary;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.function.Predicate;

@Component
public class SummaryCalculator {

    public CarChargingSummary calculateSummary(LocalDateTime time, Collection<CarChargingSession> sessions) {
        Long totalCount = calculateStoppedOrStartedOneMinuteBefore(time, sessions);
        Long startedCount = calculateStartedOneMinuteBefore(time, sessions);
        Long stoppedCount = calculateStoppedOneMinuteBefore(time, sessions);

        return new CarChargingSummary(totalCount, startedCount, stoppedCount);
    }

    private Long calculateStartedOneMinuteBefore(LocalDateTime time, Collection<CarChargingSession> sessions) {
        Predicate<CarChargingSession> predicate = chargingSession -> chargingSession.hasStartedOneMinuetBefore(time);

        return countFrequency(sessions, predicate);
    }

    private Long calculateStoppedOneMinuteBefore(LocalDateTime time, Collection<CarChargingSession> sessions) {
        Predicate<CarChargingSession> predicate = chargingSession -> chargingSession.hasStoppedOneMinuteBefore(time);

        return countFrequency(sessions, predicate);
    }

    private Long calculateStoppedOrStartedOneMinuteBefore(LocalDateTime time, Collection<CarChargingSession> sessions) {
        Predicate<CarChargingSession> predicate = chargingSession ->
                chargingSession.hasStatedOrFinishedUptToOneMinuteBefore(time);

        return countFrequency(sessions, predicate);
    }

    private long countFrequency(Collection<CarChargingSession> sessions, Predicate<CarChargingSession> predicate) {
        return sessions.stream()
                .filter(predicate)
                .count();
    }
}
