## Car charging session API 

#### Dependencies
* Java 8
* [Gradle](https://gradle.org/)

#### Running tests

`gradle test` or
`./gradlew test` (if you don't have gradle installed you may run gradlew script instead. It may take some time when running for the first time.)

#### Building the application

`gradle build` or
`./gradlew build`

#### Running the application

`gradle bootRun` or `./gradlew bootRun` - starts the application